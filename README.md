# Observations fetcher

Tool to create HTML webpage containg future SatNOGS observations

## Getting Started

Just pull repository

### Prerequisites

Python json, sqlite3, urllib2

### Usage

Edit obs_html.py and change 
* station_id to station you want to monitor.
* sqlite_file to path with SatNOGS sqlite3 data

Run with


```
python obs_html.py > somehtml.html
```
