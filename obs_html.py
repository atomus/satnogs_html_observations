#!/usr/bin/python
import urllib2, json
import sqlite3
import datetime

station_id='108'

satnogs_url = "https://network-dev.satnogs.org/api/jobs/?format=json&ground_station="+station_id
sqlite_file='/opt/satnogs_2m/jobs.sqlite'

def html_head():
    print "<html><head>"
    print "<link href=\"https://fonts.googleapis.com/css?family=Roboto|Open+Sans\" rel=\"stylesheet\">"
    print "<link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb\" crossorigin=\"anonymous\">"
    print "<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.bundle.min.js\"></script>"
    print "<style>"
    print "body {padding-bottom:10px; font-family: \'Roboto\', sans-serif;}"
    print "</style>"
    print "</head>"
    print "<body>"

def get_obs(dbfile):
    sqlite_db=sqlite3.connect(dbfile)
    dane=sqlite_db.cursor()
    sqlite_rows={}
    for obs_table in dane.execute('SELECT id,next_run_time FROM apscheduler_jobs ORDER BY next_run_time ASC'):
	if "observer" in obs_table[0]:
	    obsnum=str(obs_table[0].split("_")[1])
	    obs_date=datetime.datetime.fromtimestamp(int(obs_table[1])).strftime('%Y-%m-%d %H:%M:%S')
	    sqlite_rows[obsnum]=obs_date
    sqlite_db.close()
    print "<table class=\"table table-striped table-hover table-bordered\">"
    print "<thead class=\"thead-dark\"><tr><th>Observation no</th><th>Observation date (UTC)</th><th>Link to observation at SatNOGS</th></tr></thead>"

    if sqlite_rows:
	for obs_num, obs_date_time in sqlite_rows.iteritems():
	    print "<tr>"
	    print "<td class=\"info\" width=\"10%\">"+"<b>"+obs_num+"</b></td>"
	    print "<td class=\"success\" width=\"20%\">"+obs_date_time+" Local Time"+"</td>"
	    print "<td class=\"active\">"+"==> "+"<a href=\"https://network-dev.satnogs.org/observations/"+obs_num+"\">Observation "+obs_num+"</a>"+"</td>"
	    print "</tr>"
    else:
	print "<h3>No planned observations in near future.</h3>"
    if sqlite_rows:
    	print "</table>"


html_head()

try:
    response = urllib2.urlopen(satnogs_url)

except urllib2.URLError as e:
    print "<p><small><b>WARNING:</b> Online data fetch failed, bailing out to local cache.</small>"
    get_obs(sqlite_file)
    print "</p>"


except Exception:
    print "Unknown error"

else:
    data = json.loads(response.read())
    if data:
	print "<table class=\"table table-striped table-hover table-bordered\">"
	print "<thead class=\"thead-dark\"><tr><th>Observation no</th><th>Satellite name</th><th>Observation start</th><th>Observation end</th><th>Frequency</th><th>Rec. mode</th><th>Link to observation at SatNOGS</th></tr></thead>"
	for api_data in data:
	    obsid=str(api_data['id'])
	    tle0=str(api_data['tle0'])
	    start=str(api_data['start'])
	    stop=str(api_data['end'])
	    freq=str(api_data['frequency'])
	    mode=str(api_data['mode'])
	    if api_data:
		print "<tr>"
    		print "<td class=\"info\" width=\"10%\">"+"<b>"+obsid+"</b></td>"
    		print "<td class=\"success\" width=\"10%\">"+tle0+""+"</td>"
    		print "<td class=\"success\" width=\"10%\">"+start+""+"</td>"
    		print "<td class=\"success\" width=\"10%\">"+stop+""+"</td>"
    		print "<td class=\"success\" width=\"10%\">"+freq+"Hz"+"</td>"
    		print "<td class=\"success\" width=\"10%\">"+mode+""+"</td>"
    		print "<td class=\"active\">"+"==> "+"<a href=\"https://network-dev.satnogs.org/observations/"+obsid+"\">Observation "+obsid+"</a>"+"</td>"
    		print "</tr>"
	print "</table>"
    else:
	print "<h3>No planned observations in near future.</h3>Please check previous observations at <a href=\"https://network-dev.satnogs.org/observations/?station="+station_id+"\">station observations</a>."

print "</body></html>"